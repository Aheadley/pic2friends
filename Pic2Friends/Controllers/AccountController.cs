﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Pic2Friends.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Identity.Core;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using System.IO;
using Microsoft.AspNetCore.Hosting;

namespace Pic2Friends.Controllers
{
    public class AccountController : Controller
    {
        private readonly IWebHostEnvironment _environment;
        private readonly UserManager<IdentityUser> _userManager;
        private readonly SignInManager<IdentityUser> _signInManager;

        public AccountController(
            IWebHostEnvironment environment,
            UserManager<IdentityUser> userManager,
            SignInManager<IdentityUser> signInManager
        )
        {
            _environment = environment;
            _userManager = userManager;
            _signInManager = signInManager;
        }

        public IActionResult Register()
        {
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> Register(Register model)
        {
            if (ModelState.IsValid)
            {
                var user = new IdentityUser
                {
                    UserName = model.Email,
                    Email = model.Email,
                };

                var result = await _userManager.CreateAsync(user, model.Password);

                if (result.Succeeded)
                {
                    await _signInManager.SignInAsync(user, isPersistent: false);

                    return RedirectToAction("Login", "Account");
                }

                foreach (var error in result.Errors)
                {
                    ModelState.AddModelError("", error.Description);
                }

                ModelState.AddModelError(string.Empty, "Invalid Login Attempt");

            }
            return View(model);
        }

        [HttpGet]
        [AllowAnonymous]
        public IActionResult Login()
        {
            return View();
        }

        //This block of code allows the user to log in and redirect to the profile view
        [HttpPost]
        [AllowAnonymous]

        public async Task<IActionResult> Login(Login data)
        {
            if (ModelState.IsValid)
            {
                var result = await _signInManager.PasswordSignInAsync(
                    data.Email,
                    data.Password,
                    data.RememberMe,
                    false
                );

                if (result.Succeeded)
                {
                    return RedirectToAction("Profile", "Account");
                }

                ModelState.AddModelError(string.Empty, "Invalid Login Attempt");

            }
            return View(data);
        }

        //This block of code allows the user to logout and redirects them to the login view.
        public async Task<IActionResult> Logout()
        {
            await _signInManager.SignOutAsync();

            return RedirectToAction("Login");
        }

        [Authorize]
        public IActionResult Profile()
        {
            return View();
        }

        //This block of code is uploading the profile pic and posting it at the same time
        //This works with the profile view
        [HttpPost]
        public async Task<IActionResult> Profile(IFormFile profilepic)
        {
            var file = profilepic;

            if (file != null && file.Length > 0)
            {
                var user = await _userManager.GetUserAsync(HttpContext.User);
                // full path to file in temp location
                var filePath = Path.Combine(
                    _environment.WebRootPath,
                    "img",
                    "profilepics",
                    user.Id + ".jpg"
                );

                using (var stream = new FileStream(filePath, FileMode.Create))
                {
                    await file.CopyToAsync(stream);
                }
            }

            // Don't rely on or trust the FileName property without validation.
            return Ok();
        }
    }
}
