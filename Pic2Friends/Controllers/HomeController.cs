﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Pic2Friends.Models;
using Pic2Friends.Models.AppDBContext;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using static Microsoft.EntityFrameworkCore.DbLoggerCategory;

namespace Pic2Friends.Controllers
{
    [Authorize]
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;
        readonly AppDBContext database;
        private readonly UserManager<IdentityUser> _userManager;

        public HomeController(AppDBContext context, ILogger<HomeController> logger, UserManager<IdentityUser> userManager)
        {
            database = context;
            _logger = logger;
            _userManager = userManager;
        }

        [Authorize]
        public ActionResult Index()
        {
            ViewBag.Posts = Enumerable.Reverse(database.Posts.Include(x => x.User)).ToArray();
            ViewBag.Error = (string)TempData["Err"];
            return View();
        }

        //This block of code is ensuring that the authorize user is logged in before making a post
        //This works with the Index View.
        [HttpPost]
        [Authorize]
        public async Task<ActionResult> Index(string body)
        {
            var user = await _userManager.GetUserAsync(HttpContext.User);

            if (body != null && body.Length > 0)
            {
                database.Posts.Add(
                    new Post {User = user, Body = body});
                database.SaveChanges();
            }
            else
            {
                TempData["Err"] = "No content provided.";
            }
            return RedirectToAction("Index");
            //return Ok();
        }

        [HttpDelete("{id}")]
        [Authorize]
        public async Task<ActionResult> Index(int id)
        {
            var user = await _userManager.GetUserAsync(HttpContext.User);

            if (id > 0)
            {
                database.Posts.Remove(
                    new Post { Id = id, UserId = user.Id });
                database.SaveChanges();
            }
            else
            {
                return NotFound();
            }

            return Ok();
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
