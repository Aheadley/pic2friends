﻿CREATE TABLE [dbo].[Posts] (
    [Id]      INT              NOT NULL,
    [UserId]     NVARCHAR (MAX) NOT NULL,
    [body] NVARCHAR (MAX)   NOT NULL,
    PRIMARY KEY CLUSTERED ([Id] ASC)
);