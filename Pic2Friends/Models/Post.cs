﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.AspNetCore.Identity;
using System.Web;

namespace Pic2Friends.Models
{
    public class Post
    {
        [Key]
        public int Id { get; set; }

        public string UserId { get; set; }

        public string Body { get; set; }

        [ForeignKey("UserId")]
        public IdentityUser User { get; set; }

    }
}
